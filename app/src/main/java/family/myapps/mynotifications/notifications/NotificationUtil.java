package family.myapps.mynotifications.notifications;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */

public class NotificationUtil {

    public static boolean isNotificationAccessEnabled(Context context) {
        String notificationListenerString = Settings.Secure.getString(
                context.getContentResolver(),
                "enabled_notification_listeners");

        //Check notifications access permission
        return notificationListenerString != null && notificationListenerString.contains(context.getPackageName());
    }

    public static void startNotificationAccessActivity(Context context) {
        context.startActivity(new Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS));
    }
}
