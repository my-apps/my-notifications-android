package family.myapps.mynotifications.notifications;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import javax.inject.Inject;

import family.myapps.mynotifications.MyApplication;
import family.myapps.mynotifications.R;
import family.myapps.mynotifications.data.AppInfoRepository;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */

public class NotificationService extends NotificationListenerService {

    private static final String TAG = "NotificationService";
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    @Inject
    AppInfoRepository mAppInfoRepository;
    @Inject
    OkHttpClient.Builder mOkHttpBuilder;
    private OkHttpClient mOkHttp;
    private String mAuthBody;
    private String mBaseUrl;

    @Override
    public void onCreate() {
        super.onCreate();

        MyApplication.getAppComponent().inject(this);

        // Create an OkHttp client that authorizes every request
        mBaseUrl = getResources().getString(R.string.http_server);
        String username = getResources().getString(R.string.http_username);
        String password = getResources().getString(R.string.http_password);
        mAuthBody = "Basic " + Base64.encodeToString((username + ":" + password).getBytes(), Base64.NO_WRAP);
        mOkHttp = mOkHttpBuilder
                .addInterceptor(chain -> {
                    Request request = chain.request();
                    request = request.newBuilder()
                            .addHeader("Authorization", mAuthBody)
                            .build();
                    return chain.proceed(request);
                }).build();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);
    }

    @Override
    public void onListenerConnected() {
        super.onListenerConnected();
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        super.onNotificationPosted(sbn);
        NotificationDetails details = NotificationDetails.from(this, sbn);

        if (!mAppInfoRepository.isAppEnabled(details.packageName)) {
            return;
        }

        Request request = new Request.Builder()
                .url(mBaseUrl + "/api/v1/notification")
                .addHeader("Content-Type", "application/json")
                .post(RequestBody.create(JSON, details.toJson().toString()))
                .build();

        mOkHttp.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, "FAILED", e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.d(TAG, "SUCCESS");
            }
        });
    }

    private static class NotificationDetails {

        private static final String KEY_TITLE = "android.title";
        private static final String KEY_BODY = "android.text";

        final String packageName;
        final String appName;
        final String title;
        final String titleBig;
        final String text;
        final String textBig;

        private NotificationDetails(
                String packageName,
                String appName,
                String title,
                String titleBig,
                String text,
                String textBig) {
            this.packageName = packageName;
            this.appName = appName;
            this.title = title;
            this.titleBig = titleBig;
            this.text = text;
            this.textBig = textBig;
        }

        public static NotificationDetails from(@NonNull Context context, @NonNull StatusBarNotification sbn) {
            String packageName = sbn.getPackageName();
            PackageManager packageManager = context.getPackageManager();

            // Attempt to get app name
            String appName = "";
            try {
                ApplicationInfo info = packageManager.getApplicationInfo(packageName, PackageManager.GET_META_DATA);
                appName = packageManager.getApplicationLabel(info).toString();
            } catch (PackageManager.NameNotFoundException e) {
                Log.e(TAG, "Failed to obtain app name.", e);
            }

            // Parse the bundle for the remaining data
            Bundle bundle = sbn.getNotification().extras;
            return new NotificationDetails(
                    packageName,
                    appName,
                    bundle.getString(Notification.EXTRA_TITLE),
                    bundle.getString(Notification.EXTRA_TITLE_BIG),
                    bundle.getString(Notification.EXTRA_TEXT),
                    bundle.getString(Notification.EXTRA_BIG_TEXT));
        }

        JSONObject toJson() {
            JSONObject object = new JSONObject();
            try {
                object.put("package", packageName);
                object.put("name", appName);
                object.put("title", title);
                object.put("titleBig", titleBig);
                object.put("text", text);
                object.put("textBig", textBig);
            } catch (JSONException e) {
                Log.e(TAG, "Failed to create JSON representation.", e);
            }
            return object;
        }
    }
}
