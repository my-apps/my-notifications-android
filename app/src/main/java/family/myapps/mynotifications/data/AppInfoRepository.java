package family.myapps.mynotifications.data;

import android.arch.lifecycle.MutableLiveData;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class AppInfoRepository {

    private static final String APP_PREFIX = "app-";

    private final PackageManager mPackageManager;
    private final KeyValueDataStore mDataStore;
    private final ExecutorService mExecutor;

    AppInfoRepository(@NonNull PackageManager packageManager, @NonNull KeyValueDataStore dataStore) {
        mPackageManager = packageManager;
        mDataStore = dataStore;
        mExecutor = Executors.newSingleThreadExecutor();
    }

    public void getAllApps(final MutableLiveData<List<AppInfo>> data) {
        mExecutor.execute(() -> {
            // Get all package info
            List<PackageInfo> packageInfos = getAllPackages();

            // Parse PackageInfo's into our AppModels
            List<AppInfo> models = new ArrayList<>(packageInfos.size());
            for (PackageInfo packageInfo : packageInfos) {
                String appName = mPackageManager.getApplicationLabel(packageInfo.applicationInfo).toString();
                Drawable appIcon = packageInfo.applicationInfo.loadIcon(mPackageManager);
                AppInfo app = new AppInfo(
                        packageInfo.packageName,
                        appName,
                        appIcon,
                        mDataStore.getBoolean(key(packageInfo.packageName), true));
                models.add(app);
            }

            // Sort them so they're in the right order
            Collections.sort(models);

            // Update the live data
            data.postValue(models);
        });
    }

    public void setAppEnabled(@NonNull AppInfo app, boolean enabled) {
        mDataStore.put(key(app.packageName), enabled);
    }

    public void setAllAppsEnabled(@NonNull MutableLiveData<List<AppInfo>> data, boolean enabled) {
        mExecutor.execute(() -> {
            for (PackageInfo packageInfo : getAllPackages()) {
                mDataStore.put(key(packageInfo.packageName), enabled);
            }
            getAllApps(data);
        });
    }

    public boolean isAppEnabled(@NonNull String packageName) {
        return mDataStore.getBoolean(key(packageName), true);
    }

    @WorkerThread
    private List<PackageInfo> getAllPackages() {
        return mPackageManager.getInstalledPackages(PackageManager.GET_PERMISSIONS);
    }

    private String key(@NonNull String packageName) {
        return APP_PREFIX + packageName;
    }
}
