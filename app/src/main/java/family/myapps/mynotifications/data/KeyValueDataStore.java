package family.myapps.mynotifications.data;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */

class KeyValueDataStore {

    private static final String KEY = "my-notifications";

    private SharedPreferences mPrefs;

    KeyValueDataStore(Context context) {
        mPrefs = context.getSharedPreferences(KEY, 0);
    }

    void put(String key, boolean value) {
        mPrefs.edit().putBoolean(key, value).apply();
    }

    boolean getBoolean(String key, boolean defVal) {
        return mPrefs.getBoolean(key, defVal);
    }
}
