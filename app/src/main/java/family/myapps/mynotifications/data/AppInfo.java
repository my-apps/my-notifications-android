package family.myapps.mynotifications.data;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class AppInfo implements Comparable<AppInfo> {

    public final String packageName;
    public final String name;
    public final Drawable icon;
    public final boolean enabled;


    AppInfo(String packageName, String name, Drawable icon, boolean enabled) {
        this.packageName = packageName;
        this.name = name;
        this.icon = icon;
        this.enabled = enabled;
    }

    @Override
    public int compareTo(@NonNull AppInfo appInfo) {
        return this.name.compareTo(appInfo.name);
    }
}
