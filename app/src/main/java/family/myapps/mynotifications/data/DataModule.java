package family.myapps.mynotifications.data;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
@Module
public class DataModule {

    @Provides
    @Singleton
    KeyValueDataStore provideKeyValueDataStore(@NonNull Context context) {
        return new KeyValueDataStore(context);
    }

    @Provides
    @Singleton
    AppInfoRepository provideAppInfoRepository(@NonNull PackageManager packageManager, @NonNull KeyValueDataStore dataStore) {
        return new AppInfoRepository(packageManager, dataStore);
    }
}
