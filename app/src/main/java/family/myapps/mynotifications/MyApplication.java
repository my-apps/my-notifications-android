package family.myapps.mynotifications;

import android.app.Application;
import android.support.annotation.NonNull;

import com.facebook.stetho.Stetho;

import family.myapps.mynotifications.di.AppComponent;
import family.myapps.mynotifications.di.AppModule;
import family.myapps.mynotifications.di.DaggerAppComponent;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class MyApplication extends Application {

    private static AppComponent sAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
        sAppComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }

    @NonNull
    public static AppComponent getAppComponent() {
        return sAppComponent;
    }
}
