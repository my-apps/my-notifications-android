package family.myapps.mynotifications;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import family.myapps.mynotifications.notifications.NotificationUtil;
import family.myapps.mynotifications.settings.AppListFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container, AppListFragment.create())
                .commit();
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Get system notification permissions
        if (!NotificationUtil.isNotificationAccessEnabled(this)) {
            NotificationUtil.startNotificationAccessActivity(this);
        }
    }
}
