package family.myapps.mynotifications.ui;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import javax.inject.Provider;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class SpecificViewModelProviderFactory<V> extends ViewModelProvider.NewInstanceFactory {

    private Provider<V> mViewModelProvider;

    public SpecificViewModelProviderFactory(@NonNull Provider<V> viewModelProvider) {
        mViewModelProvider = viewModelProvider;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        try {
            return modelClass.cast(mViewModelProvider.get());
        } catch (ClassCastException e) {
            return super.create(modelClass);
        }
    }
}
