package family.myapps.mynotifications.ui;

import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.support.v4.app.Fragment;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class BaseFragment extends Fragment implements LifecycleRegistryOwner {

    private LifecycleRegistry mRegistry = new LifecycleRegistry(this);

    @Override
    public LifecycleRegistry getLifecycle() {
        return mRegistry;
    }
}
