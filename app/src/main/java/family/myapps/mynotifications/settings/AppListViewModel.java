package family.myapps.mynotifications.settings;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import java.util.List;

import family.myapps.mynotifications.data.AppInfoRepository;
import family.myapps.mynotifications.data.AppInfo;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */

public class AppListViewModel extends ViewModel {

    private final AppInfoRepository mRepo;
    private MutableLiveData<List<AppInfo>> mApps;

    AppListViewModel(@NonNull AppInfoRepository repository) {
        mRepo = repository;
    }

    LiveData<List<AppInfo>> getAllApps() {
        if (mApps == null) {
            mApps = new MutableLiveData<>();
            mRepo.getAllApps(mApps);
        } else {
            refreshApps();
        }
        return mApps;
    }

    void refreshApps() {
        if (mApps == null) {
            mApps = new MutableLiveData<>();
        }
        mRepo.getAllApps(mApps);
    }

    void setAppEnabled(@NonNull AppInfo app, boolean enabled) {
        mRepo.setAppEnabled(app, enabled);
        refreshApps();
    }

    void setAllAppsEnabled(boolean enabled) {
        if (mApps == null) {
            mApps = new MutableLiveData<>();
        }
        mRepo.setAllAppsEnabled(mApps, enabled);
    }
}
