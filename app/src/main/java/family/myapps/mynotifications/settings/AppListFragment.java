package family.myapps.mynotifications.settings;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import family.myapps.mynotifications.MyApplication;
import family.myapps.mynotifications.R;
import family.myapps.mynotifications.data.AppInfo;
import family.myapps.mynotifications.ui.BaseFragment;
import family.myapps.mynotifications.ui.SpecificViewModelProviderFactory;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */

public class AppListFragment extends BaseFragment implements Observer<List<AppInfo>>,AppListAdapter.Listener {

    private AppListAdapter mAdapter;

    private SwipeRefreshLayout mPtrContainer;
    private RecyclerView mAppList;
    private View mEnableAllButton;
    private View mDisableAllButton;

    @Inject
    SpecificViewModelProviderFactory<AppListViewModel> mViewModelProviderFactory;
    private AppListViewModel mViewModel;

    public static AppListFragment create() {
        return new AppListFragment();
    }

    public AppListFragment() {
        MyApplication.getAppComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_app_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPtrContainer = view.findViewById(R.id.ptr_container);
        mAppList = view.findViewById(R.id.app_list);
        mAppList.setLayoutManager(new LinearLayoutManager(view.getContext()));
        mEnableAllButton = view.findViewById(R.id.enable_all);
        mDisableAllButton = view.findViewById(R.id.disable_all);
    }

    @Override
    public void onStart() {
        super.onStart();

        // Instantiate adapter
        mAdapter = new AppListAdapter();
        mAdapter.setListener(this);
        mAppList.setAdapter(mAdapter);

        // Listen to changes to the app list
        mPtrContainer.setRefreshing(true);
        mViewModel = ViewModelProviders.of(this, mViewModelProviderFactory).get(AppListViewModel.class);
        mViewModel.getAllApps().observe(this, this);

        // Listen to PTR
        mPtrContainer.setOnRefreshListener(() -> mViewModel.refreshApps());

        // Enable/Disable buttons
        mEnableAllButton.setOnClickListener(view -> mViewModel.setAllAppsEnabled(true));
        mDisableAllButton.setOnClickListener(view -> mViewModel.setAllAppsEnabled(false));
    }

    @Override
    public void onChanged(@Nullable List<AppInfo> appInfos) {
        mPtrContainer.setRefreshing(false);
        mAdapter.setData(appInfos);
    }

    @Override
    public void onAppToggled(AppInfo app, boolean enabled) {
        mViewModel.setAppEnabled(app, enabled);
    }
}
