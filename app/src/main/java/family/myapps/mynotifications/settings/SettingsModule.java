package family.myapps.mynotifications.settings;

import android.support.annotation.NonNull;

import javax.inject.Provider;

import dagger.Module;
import dagger.Provides;
import family.myapps.mynotifications.data.AppInfoRepository;
import family.myapps.mynotifications.ui.SpecificViewModelProviderFactory;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
@Module
public class SettingsModule {

    @Provides
    public AppListViewModel provideAppListViewModel(@NonNull AppInfoRepository repo) {
        return new AppListViewModel(repo);
    }

    @Provides
    public SpecificViewModelProviderFactory<AppListViewModel> provideAppListViewModelProviderFactory(@NonNull Provider<AppListViewModel> provider) {
        return new SpecificViewModelProviderFactory<>(provider);
    }
}
