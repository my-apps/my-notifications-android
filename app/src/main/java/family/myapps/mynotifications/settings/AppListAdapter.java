package family.myapps.mynotifications.settings;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import family.myapps.mynotifications.R;
import family.myapps.mynotifications.data.AppInfo;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class AppListAdapter extends RecyclerView.Adapter<AppListAdapter.AppListViewHolder> {

    private final List<AppInfo> mData;
    @Nullable
    private Listener mListener;

    AppListAdapter() {
        mData = new ArrayList<>();
        setHasStableIds(true);
    }

    @Override
    public AppListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_app_list, parent, false);
        return new AppListViewHolder(view, (app, enabled) -> {
            if (mListener != null) {
                mListener.onAppToggled(app, enabled);
            }
        });
    }

    @Override
    public void onBindViewHolder(AppListViewHolder holder, int position) {
        holder.bind(mData.get(position));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public void onViewRecycled(AppListViewHolder holder) {
        holder.recycle();
    }

    @Override
    public long getItemId(int position) {
        return mData.get(position).packageName.hashCode();
    }

    public void setData(List<AppInfo> apps) {
        mData.clear();
        mData.addAll(apps);
        notifyDataSetChanged();
    }

    public void setListener(@Nullable Listener listener) {
        mListener = listener;
    }

    public interface Listener {
        void onAppToggled(AppInfo app, boolean enabled);
    }

    static class AppListViewHolder extends RecyclerView.ViewHolder {

        private final TextView mAppNameView;
        private final ImageView mAppIconView;
        private final Switch mAppEnabledToggle;
        @Nullable
        private final Listener mListener;

        AppListViewHolder(View itemView, @Nullable Listener listener) {
            super(itemView);
            mAppNameView = itemView.findViewById(R.id.app_name);
            mAppIconView = itemView.findViewById(R.id.app_icon);
            mAppEnabledToggle = itemView.findViewById(R.id.app_enabled);
            mListener = listener;
        }

        void bind(@NonNull AppInfo app) {
            mAppNameView.setText(app.name);
            mAppIconView.setImageDrawable(app.icon);
            mAppEnabledToggle.setChecked(app.enabled);
            mAppEnabledToggle.setOnCheckedChangeListener((button, enabled) -> {
                if (mListener != null) {
                    mListener.onAppToggled(app, enabled);
                }
            });
        }

        void recycle() {
            mAppEnabledToggle.setOnCheckedChangeListener(null);
        }
    }
}
