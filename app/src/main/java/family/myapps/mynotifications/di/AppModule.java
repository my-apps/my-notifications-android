package family.myapps.mynotifications.di;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
@Module
public class AppModule {

    private final Context mContext;

    public AppModule(@NonNull Context context) {
        mContext = context;
    }

    @Provides
    Context provideContext() {
        return mContext;
    }

    @Provides
    PackageManager providePackageManager(@NonNull Context context) {
        return context.getPackageManager();
    }
}
