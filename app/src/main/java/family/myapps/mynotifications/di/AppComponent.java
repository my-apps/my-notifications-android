package family.myapps.mynotifications.di;

import javax.inject.Singleton;

import dagger.Component;
import family.myapps.mynotifications.data.DataModule;
import family.myapps.mynotifications.network.NetworkModule;
import family.myapps.mynotifications.notifications.NotificationService;
import family.myapps.mynotifications.settings.AppListFragment;
import family.myapps.mynotifications.settings.SettingsModule;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
@Singleton
@Component(modules = { AppModule.class, DataModule.class, NetworkModule.class, SettingsModule.class })
public interface AppComponent {
    void inject(AppListFragment fragment);
    void inject(NotificationService service);
}
