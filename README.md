# My Notifications

The Android client for a Pushbullet-like system that allows you to see your
phone's notifications on other clients in real-time. It must be paired with an
instance of the [My Notifications
Server](https://gitlab.greyson.house/my-apps/my-notifications-server).

## Setup

At the moment you must add the following string resources to your project: 

* `http_server`: The base URL of your My Notifications server, without the
  trailing slash, e.g. https://example.com
* `http_username`: The username for your My Notifications server.
* `http_password`: The password for your My Notifications server. 

I recommend adding them in res file called `secret.xml`, which is already 
ignored in the `.gitignore`. 

This is not very secure. See the disclaimer.

## Disclaimer

This is not production-ready in any sense, and should not be distributed in it's
current form. The security is very basic.
